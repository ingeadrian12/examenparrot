package com.parrot.examenparrot.common

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

open class FragmentBase(idResource: Int) :Fragment(idResource) {
     val navigation by lazy { findNavController() }
}