package com.parrot.examenparrot.common

import com.parrot.examenparrot.App
import com.parrot.examenparrot.BuildConfig
import com.parrot.examenparrot.network.manager.ManagerNetwork

open class RepositoryBase {
     val application = App.instance
     var services = ManagerNetwork.services(BuildConfig.BASE_URL)
}