package com.parrot.examenparrot.data.repository

import com.parrot.examenparrot.common.RepositoryBase
import com.parrot.examenparrot.data.request.LoginRequest
import com.parrot.examenparrot.data.response.LoginResponse
import java.lang.Exception

class LoginRepository:RepositoryBase() {

    private  var loginResponse: LoginResponse? = null

    suspend fun login(loginRequest: LoginRequest): LoginResponse? {

        try {
     val login = services.login(loginRequest)

            if (login.isSuccessful){
               login.body().let {
                  loginResponse = login.body()!!
               }
            }

        }
        catch (e:Exception){
           return  loginResponse

        }

        return loginResponse
    }
}