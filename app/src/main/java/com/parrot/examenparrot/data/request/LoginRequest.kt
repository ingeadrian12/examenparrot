package com.parrot.examenparrot.data.request

data class LoginRequest(
    var password: String,
    var username: String
)