package com.parrot.examenparrot.data.response

data class LoginResponse(val access: String, val refresh: String)