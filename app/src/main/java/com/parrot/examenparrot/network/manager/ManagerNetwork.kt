package com.parrot.examenparrot.network.manager

import com.parrot.examenparrot.network.services.Services
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ManagerNetwork {

fun services(baseUrl:String):Services{

    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(makeOkHttpClient())
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(Services::class.java)

}


    private fun makeOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(makeLoggingInterceptor())
            .connectTimeout(90, TimeUnit.SECONDS)
            .readTimeout(90, TimeUnit.SECONDS)
            .writeTimeout(90, TimeUnit.SECONDS)
            .build()
    }

    private fun makeLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }
}