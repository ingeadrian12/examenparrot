package com.parrot.examenparrot.network.services

import com.parrot.examenparrot.data.request.LoginRequest
import com.parrot.examenparrot.data.response.LoginResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface Services {

    @POST("auth/token")
    suspend fun login(@Body loginRequest: LoginRequest): Response<LoginResponse>
}