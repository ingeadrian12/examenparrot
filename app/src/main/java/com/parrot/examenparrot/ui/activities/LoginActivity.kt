package com.parrot.examenparrot.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.Navigation
import com.parrot.examenparrot.R
import com.parrot.examenparrot.databinding.ActivityMainBinding

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val mNavigation by lazy { Navigation.findNavController(binding.root.getViewById(R.id.nav_graph)) }

    }
}