package com.parrot.examenparrot.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.parrot.examenparrot.R
import com.parrot.examenparrot.common.FragmentBase
import com.parrot.examenparrot.databinding.FragmentDetailBinding
import com.parrot.examenparrot.databinding.FragmentLoginBinding

class DetailFragment : FragmentBase(R.layout.fragment_detail) {

    private var detailFragment: FragmentDetailBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentDetailBinding.bind(view)
        detailFragment = binding

    }






}