package com.parrot.examenparrot.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.lifecycle.*
import com.parrot.examenparrot.R
import com.parrot.examenparrot.common.FragmentBase
import com.parrot.examenparrot.data.request.LoginRequest


import com.parrot.examenparrot.databinding.FragmentLoginBinding
import com.parrot.examenparrot.viewmodel.LoginFragmentViewModel

class LoginFragment : FragmentBase(R.layout.fragment_login) {

    private var fragmentLogin: FragmentLoginBinding? = null
    private lateinit var viewModel: LoginFragmentViewModel
    private var loginRequest :LoginRequest? =null
   private val binding: FragmentLoginBinding? = null



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val  binding = FragmentLoginBinding.bind(view)
       fragmentLogin = binding


       binding.button.setOnClickListener {
            loginRequest = LoginRequest(binding.editTextTextPassword.text.toString(),binding.editTextTextPersonName.text.toString())

            viewModel.login(loginRequest!!)

        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoginFragmentViewModel::class.java)
        retrieveResponseServices()
    }

    private val observerLogin = Observer<Any>(){
        netFragment()

    }

    private fun retrieveResponseServices() {
        viewModel.response.observe(viewLifecycleOwner,observerLogin)
    }
    private fun netFragment(){
        navigation.navigate(R.id.action_loginFragment_to_detailFragment)
    }

}




