package com.parrot.examenparrot.viewmodel
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.parrot.examenparrot.data.repository.LoginRepository
import com.parrot.examenparrot.data.request.LoginRequest
import com.parrot.examenparrot.data.response.LoginResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginFragmentViewModel:ViewModel() {
    private val loginRepository by lazy { LoginRepository() }
    var response = MutableLiveData<Any>()


    fun login (loginRequest: LoginRequest){

        GlobalScope.launch {
            val res = loginRepository.login(loginRequest)
            withContext(Dispatchers.IO){
                response.postValue(res!!)

            }
        }
    }

}